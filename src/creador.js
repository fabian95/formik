import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';
 

class Creador extends React.Component{
    enviarForm(valores,){
        console.log(valores);
        
        fetch(
            'http://localhost:3001/insertar',
            {
                method:'POST',
                headers:{'Content-type':'application/json',
                  'cors':'Access-Control-Allow-Origin'},
                body:JSON.stringify({

                    user:{
                        idLUGARES:valores.idLUGARES,
                        NOMBRE:valores.NOMBRE,
                        BARRIO:valores.BARRIO,
                        ACTIVIDAD:valores.ACTIVIDAD,
                        LATITUD:valores.LATITUD,
                        LONGITUD:valores.LONGITUD


                    }
                }),
            
            }
        );


    };

    render(){

        let elemento=<Formik
           initialValues={
               {
                   idLUGARES:'',
                   NOMBRE:'',
                   BARRIO:'',
                   ACTIVIDAD:'',
                   LATITUD:'',
                   LONGITUD:''
                   }

               }
               onSubmit={this.enviarForm} //click llamar funcion
               validationSchema={ Yup.object().shape(
                   {
                       idLUGARES:Yup.number().typeError('debe ser un numero').required('obligatorio'),
                       NOMBRE:Yup.string().typeError().required('obligatorio'),
                       BARRIO:Yup.string().typeError().required('obligatorio'),
                       ACTIVIDAD:Yup.string().typeError().required('obligatorio')
                   }

               )

               }
               >
                   <Container className='p-3'> 
                       <Form >
                           <div className="form-group">
                               <label htmlFor="idLUGARES">idLUGARES</label>
                               <Field name="idLUGARES" type="text" className="form-control"/>
                               <ErrorMessage name="idLUGARES" className="invalid.feedback"></ErrorMessage>

                           </div>
                           <div className="form-group">
                           <label htmlFor="NOMBRE">NOMBRE</label>
                           <Field name="NOMBRE" type="text" className="form-control"/>
                           <ErrorMessage name="NOMBRE" className="invalid.feedback"></ErrorMessage>

                           </div>
                           <div className="form-group">
                           <label htmlFor="BARRIO">BARRIO</label>
                           <Field name="BARRIO" type="text" className="form-control"/>
                           <ErrorMessage name="BARRIO" className="invalid.feedback"></ErrorMessage>

                           </div>
                           <div className="form-group">
                           <label htmlFor="ACTIVIDAD">ACTIVIDAD</label>
                           <Field name="ACTIVIDAD" type="text" className="form-control"/>
                           <ErrorMessage name="ACTIVIDAD" className="invalid.feedback"></ErrorMessage>

                           </div>
                           <div className="form-group">
                           <label htmlFor="LATITUD">LATITUD</label>
                           <Field name="LATITUD" type="text" className="form-control"/>
                           <ErrorMessage name="LATITUD" className="invalid.feedback"></ErrorMessage>

                           </div>
                           <div className="form-group">
                           <label htmlFor="LONGITUD">LONGITUD</label>
                           <Field name="LONGITUD" type="text" className="form-control"/>
                           <ErrorMessage name="LONGITUD" className="invalid.feedback"></ErrorMessage>

                           </div>
                           <div className="form-group">
                               <Button type="submit" className= "btn btn-primary m-4">Aceptar</Button> 
                               <Button type="reset" className= "btn btn-secondary">Cancelar</Button>

                           </div>
                       </Form>
                   </Container>
               </Formik>;

               return elemento;
           } 

    };

export default Creador;